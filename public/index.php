<?php
define('REAL_PATH', realpath(dirname(__FILE__)));

require REAL_PATH . '/../' . 'app/System/Application.php';

$app = new app\System\Application(REAL_PATH . '/../');
$app->registerAutoloader();

$response = $app->handle();

$response->send();
