<?php

namespace app\Exceptions;

use app\System\Exceptions\ExceptionHandler as Handler;
use app\System\Http\Responses\ResponseHandler as Response;
use app\System\Exceptions\NotFoundHttpException;
use app\Helpers\ResponseBuilder;

class ExceptionHandler extends Handler
{
    public function report(\Exception $e)
    {
        $this->logger->error($e);
    }

    public function render($request, $e)
    {
        $statusCode = 400;
        $message = $e->getMessage();

        if ($e instanceof NotFoundHttpException) {
            $statusCode = 404;
            $message = !empty($e->getMessage()) ? $e->getMessage() : 'Not found';    
        }
        
        $response = ResponseBuilder::failure($statusCode, $message);
        return Response::json($response, $statusCode);
    }
}