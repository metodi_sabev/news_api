<?php

$router->get('/', function () {
    throw new \Exception("Not Found", 1);
    
});

$router->group(['prefix' => 'api/v1'], function($router) {
    $router->get('news/', 'NewsController@index');
    $router->post('news', 'NewsController@store');
    $router->get('news/:id', 'NewsController@show');
    $router->put('news/:id', 'NewsController@update');
    $router->delete('news/:id', 'NewsController@destroy');
});
