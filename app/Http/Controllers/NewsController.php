<?php

namespace app\Http\Controllers;

use app\System\Http\Controllers\Controller;
use app\System\Http\Responses\ResponseHandler as Response;
use app\System\Validation\Validator;
use app\Helpers\ResponseBuilder;
use app\Models\Articles;

class NewsController extends Controller
{
    public function index()
    {
        $news = new Articles;
        $data = $news->all();

        $result = [];
        foreach ($data as $key => $item) {
            $result[] = [
                'id' => $item->id,
                'title' => $item->headline,
                'content' => $item->content,
            ];
        }

        $response = ResponseBuilder::successWithData($result);
        return Response::json($response);
    }

    public function store($request)
    {
        $data = $request->getRaw();
        $data = json_decode($data, 1);

        $validation = new Validator($data, [
            'headline' => ['required', 'string', 'max:255'],
            'content' => ['required', 'string']
        ]);

        if ($validation->fails()) {
            $response = ResponseBuilder::failure(400, $validation->getMessages());
            return Response::json($response);
        }

        $news = new Articles;

        try {
            $news->create($data);
        } catch (\Exception $e) {
            $response = ResponseBuilder::error(500, $e->getMessage());
            return Response::json($response, 500);
        }

        $response = ResponseBuilder::success();
        return Response::json($response);
    }

    public function show($request, $id)
    {
        $news = new Articles;

        try {
            $data = $news->where((int) $id);
        } catch (\Exception $e) {
            $response = ResponseBuilder::failure(400, $e->getMessage());
            return Response::json($response, 400);
        }

        $response = ResponseBuilder::successWithData([
            'id' => $data->id,
            'title' => $data->headline,
            'content' => $data->content,
        ]);

        return Response::json($response);
    }

    public function update($request, $id)
    {
        $data = $request->getRaw();
        $data = json_decode($data, 1);

        $validation = new Validator($data, [
            'headline' => ['string', 'max:255'],
            'content' => ['string'],
        ]);

        if ($validation->fails()) {
            $response = ResponseBuilder::failure(400, $validation->getMessages());
            return Response::json($response);
        }

        $news = new Articles;

        try {
            $news->update($id, $data);
        } catch (\Exception $e) {
            $response = ResponseBuilder::error(500, $e->getMessage());
            return Response::json($response, 500);
        }

        $response = ResponseBuilder::success();
        return Response::json($response);
    }

    public function destroy($request, $id)
    {
        $news = new Articles;

        try {
            $news->delete($id);
        } catch (\Exception $e) {
            $response = ResponseBuilder::error(500, $e->getMessage());
            return Response::json($response, 500);
        }

        $response = ResponseBuilder::success();
        return Response::json($response);
    }
}