<?php

namespace app\System;

class NamespaceLoader
{
    public static function load($namespace) {
        $fileName = REAL_PATH . '/../' . str_replace('\\', '/', $namespace) . '.php';
        $className = explode('\\', $namespace);
        $className = $className[count($className)-1];

        if (file_exists($fileName)) {
            include($fileName);
            if (class_exists($className)) {
                return true;
            }
        }

        return false;
    }
}

use app\System\DI\Container;

class Application extends NamespaceLoader
{
    protected $basepath;

    protected static $settings;

    protected $container;

    public function __construct($basepath)
    {
        $this->setBasePath($basepath);
    }

    public function registerAutoloader()
    {
        spl_autoload_register('app\System\NamespaceLoader::load');
    }

    private function setBasePath($basepath)
    {
        $this->basepath = $basepath;
    }

    public function getBasePath()
    {
        return $this->basepath;
    }

    public function getSettings()
    {
        return $this->settings;
    }

    public function handle($catch = true)
    {
        try {
            $response = $this->run();
        } catch (\Exception $e) {
            return $this->handleException($e);
        }

        return $response;
    }

    public function run()
    {
        $this->container = new Container;
        
        $this->container->singleton('Request', 'app\System\Http\Request');

        $this->container->set('ExceptionHandler', 'app\Exceptions\ExceptionHandler');

        $this->container->set('Logger', 'app\System\Logger\Logger');

        $this->container->get('Router', function () {
            $router = new \app\System\Http\Routing\Router();

            include REAL_PATH . '/../app/Http/routes.php';

            return $router;
        });

        $this->container->singleton('DB', 'app\System\DB\Connector');

        // $this->container->singleton('Response', 'app\System\Http\Responses\Response');

        return $this->dispatch();
    }

    protected function dispatch()
    {
        $router = $this->container->get('Router');
        $request = $this->container->get('Request');

        $matchedRoutes = $router->handle($request);
        $response = $matchedRoutes[0]->dispatch($request);

        if (empty($response)) {
            throw new \Exception('Something went wrong', 1);
        }

        return $response;
    }

    private function handleException($e)
    {
        $this->reportException($e);

        return $this->renderException($this->container->get('Request'), $e);
    }

    protected function reportException($e)
    {
        $handler = $this->container->get('ExceptionHandler', [
            'logger' => $this->container->get('Logger'),
        ]);

        $handler->report($e);
    }

    protected function renderException($request, $e)
    {
        $handler = $this->container->get('ExceptionHandler', [
            'response' => $this->container->get('Response'),
        ]);

        return $handler->render($request, $e);
    }

    /**
     * Collect the environment settings from the file
     * @return array
     */
    public static function collectEnvSettings()
    {
        if (self::$settings) {
            return self::$settings;
        }

        $envPath = REAL_PATH . '/../.env';

        if (file_exists($envPath)) {
            $envSettings = file_get_contents($envPath);

            if (!empty($envSettings)) {
                self::$settings = $envSettings;

                return json_decode($envSettings, true);    
            }
        }

        return [];
    }
}
