<?php

namespace app\System\DB;

use app\System\Application;

class Connector
{
    protected static $instance;

    protected $settings;

    protected $connection;

    public function __construct()
    {
        $this->settings = $this->getEnvSettings();

        $this->connection = $this->connect();
    }

    public function __destruct()
    {
        $this->closeConnection();
    }

    public function getConnection()
    {
        return $this->connection;
    }

    public function closeConnection()
    {
        $connection = $this->getConnection();

        $connection->close();
    }

    public function connect()
    {
        $connection = new \mysqli(
            $this->settings['host'] ?: 'localhost',
            $this->settings['username'] ?: 'root',
            $this->settings['password'] ?: '',
            $this->settings['database'] ?: ''
        );

        if ($connection->connect_error) {
            throw new \Exception("Connection to the DB failed", 1);
        }

        return $connection;
    }

    private function getEnvSettings()
    {
        $settings = Application::collectEnvSettings();

        if (!empty($settings['db'])) {
            return $settings['db'];
        }

        throw new \Exception("Wrong or missing db settings", 1);
    }

    public static function getConnector()
    {
        if (self::$instance) {
            return self::$instance;
        }

        self::$instance = new self;

        return self::$instance;
    }
}