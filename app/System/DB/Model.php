<?php

namespace app\System\DB;

class Model
{
    private $connection;

    protected $timestamps = true;

    protected $created_at = 'created_at';

    protected $updated_at = 'updated_at';

    protected $deleted_at = 'deleted_at';

    public function __construct($attrs = null)
    {
        $connector = Connector::getConnector();

        $this->connection = $connector->getConnection();

        if (!empty($attrs)) {
            $this->attributes = $attrs;
        }
    }

    public function __get($key)
    {
        return $this->attributes[$key] ?: null;
    }

    private function fetchObject($result)
    {
        if (!empty($this->connection->error)) {
            throw new \Exception("SQL Error: " . $this->connection->error, 1);
        }

        $resultSet = [];
        if ($result) {
            while ($obj = $result->fetch_assoc()) {
                $className = get_class($this);
                $resultSet[] = new $className($obj);
            }
        }

        return count($resultSet) == 1 ? $resultSet[0] : $resultSet;
    }

    public function all()
    {
        $sql = 'SELECT '. implode(', ', $this->fillable) .' FROM ' . $this->tableName . ' WHERE deleted_at is NULL';

        $result = $this->connection->query($sql);

        return $this->fetchObject($result);
    }

    public function where($args)
    {
        $sql = '';

        if ("integer" === gettype($args)) {
            $sql .= ' id = ' . $args;
        }

        /** @ToDo Add array check and implementation here */

        if (empty($sql)) {
            throw new \Exception("Error Processing Data", 1);
        }

        $sql = 'SELECT '. implode(', ', $this->fillable) .' FROM ' . $this->tableName . ' WHERE ' . $sql . ' and ' . $this->deleted_at . ' is NULL';

        $result = $this->connection->query($sql);

        return $this->fetchObject($result);
    }

    protected function sanitize($item)
    {
        $item = htmlspecialchars($item);
        $item = strip_tags($item);
        $item = $this->connection->real_escape_string($item);

        return $item;
    }

    protected function handle($action, $params = [], $id = null)
    {
        $sql = '';
        foreach ($params as $key => $value) {
            if (in_array($key , $this->fillable)) {
                $sql .= (empty($sql) ? '' : ', ') . $this->sanitize($key) . ' = \'' . $this->sanitize($value) . '\'';
            }
        }

        if ($this->timestamps) {
            $sql .= ', ' . ($action == 'create' ? $this->created_at : $this->updated_at) . ' = NOW()';
        }

        if ($action == 'create') {
            $sql = 'INSERT INTO ' . $this->tableName . ' SET ' . $sql;
        } elseif ($action == 'update') {
            $sql = 'UPDATE ' . $this->tableName . ' SET ' . $sql . ' WHERE id = ' . $id;
        } elseif ($action == 'delete') {
            $sql = 'UPDATE ' . $this->tableName . ' SET ' . $this->deleted_at . ' = NOW() WHERE id = ' . $id;
        } else {
            throw new \Exception("Undefined action", 1);
        }

        $result = $this->connection->query($sql);

        if (!empty($this->connection->error)) {
            throw new \Exception("SQL Error: " . $this->connection->error, 1);
        }

        return $this->where($this->connection->insert_id);
    }

    public function create($params)
    {
        return $this->handle('create', $params);
    }

    public function update($id, $params)
    {
        return $this->handle('update', $params, $id);
    }

    public function delete($id)
    {
        return $this->handle('delete', [], $id);
    }
}