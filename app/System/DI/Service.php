<?php

namespace app\System\DI;

class Service
{
    protected $name;

    protected $definition;

    protected $shared;

    protected $resolved = false;

    protected $sharedInstance;

    public function __construct($name, $definition, $shared = false)
    {
        $this->name = $name;
        $this->definition = $definition;
        $this->shared = $shared;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setShared($shared)
    {
        $this->shared = $shared;
    }

    public function isShared()
    {
        return $this->shared;
    }

    public function setSharedInstance($sharedInstance)
    {
        $this->sharedInstance = $sharedInstance;
    }

    public function setDefinition($definition)
    {
        $this->definition = $definition;
    }

    public function getDefinition()
    {
        return $this->definition;
    }

    public function isResolved()
    {
        return $this->resolved;
    }

    public function resolve($parameters = null)
    {
        if ($this->isShared()) {
            if (!empty($this->sharedInstance)) {
                return $this->sharedInstance;
            }
        }

        if (is_string($this->definition)) {
            if (class_exists($this->definition)) {
                if (is_array($parameters) && !empty($parameters)) {
                    $instance = new $this->definition($parameters);
                } else {
                    $instance = new $this->definition;
                }
            }
        } elseif ($this->definition instanceof \Closure) {
            $instance = call_user_func($this->definition);
        }

        if (empty($instance)) {
            throw new \Exception('Service ' . $this->name . ' can not be resolved', 1);
        }

        if ($this->shared) {
            $this->sharedInstance = $instance;
        }

        $this->resolved = true;

        return $instance;
    }

}