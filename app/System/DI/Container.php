<?php

namespace app\System\DI;

class Container
{
    /**
     * The container's bindings
     * 
     * @var array
     */
    protected $services = [];

    public function set($name, $definition = null, $shared = false)
    {
        $name = $this->normalize($name);
        $definition = $this->normalize($definition);

        if (is_null($definition)) {
            $definition = $name;
        }

        $service = new Service($name, $definition, $shared);
        $this->services[$name] = $service;
    }

    public function singleton($name, $definition = null)
    {
        $this->set($name, $definition, true);
    }

    public function get($name, $parameters = null)
    {
        if (!empty($this->services[$name])) {
            $instance = $this->services[$name]->resolve($parameters);
        }

        if ($parameters instanceof \Closure) {
            $this->set($name, $parameters);
            $instance = $this->services[$name]->resolve();
        }

        if (empty($instance)) {
            throw new \Exception('Service "' . $name . '" wasn\'t found in the dependency injection container', 1);
        }

        return $instance;
    }

    public function has($name)
    {
        return isset($this->services[$name]);
    }

    public function getServices()
    {
        return $this->services;
    }

    protected function normalize($service)
    {
        return is_string($service) ? ltrim($service, '\\') : $service;
    }
}