<?php

namespace app\System\Validation;

class Validator
{
    protected $data;
    protected $rules;
    protected $failed = false;
    protected $messages = [];

    public function __construct($data, $rules)
    {
        $this->data = $data;
        $this->rules = $rules;
    }

    public function fails()
    {
        $rules = $this->rules;
        $data = $this->data;

        $messages = [];
        foreach ($rules as $key => $rule) {
            foreach ($rule as $item) {
                if ("required" == $item) {
                    if (empty($data[$key])) {
                        $this->failed = true;
                        $messages[] = 'The ' . $key . ' is required';
                    }
                }

                if ("string" == $item) {
                    if (!is_string($data[$key])) {
                        $this->failed = true;
                        $messages[] = 'The ' . $key . ' must be a string';
                    }
                }

                if ("integer" == $item) {
                    if (!is_int($data[$key])) {
                        $this->failed = true;
                        $messages[] = 'The ' . $key . ' must be an integer';
                    }
                }

                if (strpos($item, ":") !== false) {
                    $item = explode(":", $item);
                    if ($item[0] == 'max') {
                        if (strlen($data[$key]) > $item[1]) {
                            $this->failed = true;
                            $messages[] = 'The ' . $key . ' must be max ' . $item[1] . ' long';
                        }
                    } elseif ($item[0] == 'min') {
                        if (strlen($data[$key]) < $item[1]) {
                            $this->failed = true;
                            $messages[] = 'The ' . $key . ' must be at least ' . $item[1] . ' long';
                        }
                    }
                }
            }
        }

        $this->messages = $messages;
        return $this->failed;
    }

    public function getMessages()
    {
        return $this->messages;
    }
}