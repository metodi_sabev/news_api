<?php

namespace app\System\Exceptions;

use app\System\Http\Responses\ResponseHandler as Response;

class ExceptionHandler
{
    protected $logger;

    protected $response;

    public function __construct($params)
    {
        if (is_array($params)) {
            if (!empty($params['logger'])) {
                $this->logger = $params['logger'];
            }

            if (!empty($params['response'])) {
                $this->response = $params['response'];
            }
        }
    }

    public function report(\Exception $e)
    {
        $this->logger->error($e);
    }

    public function render($request, $e)
    {
        $statusCode = 400;
        $message = $e->getMessage();

        if ($e instanceof NotFoundHttpException) {
            $statusCode = 404;
            $message = !empty($e->getMessage()) ? $e->getMessage() : 'Not found';    
        }
        
        return Response::json($message, $statusCode);
    }
}