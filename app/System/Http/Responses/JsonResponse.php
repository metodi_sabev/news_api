<?php

namespace app\System\Http\Responses;

class JsonResponse extends Response
{
    protected $encodingOptions = 15;

    public function __construct($status = 200, $data = null, $headers = [])
    {
        parent::__construct('', $status, $headers);

        if ($data === null) {
            $data = [];
        }

        $this->setData($data);
    }

    public function setData($data = [])
    {
        $this->data = json_encode($data, $this->encodingOptions);

        return $this->update();
    }

    protected function update()
    {
        if (!$this->headers->has('Content-Type') || $this->headers->get('Content-Type') === 'text/javascript') {
            $this->headers->set('Content-Type', 'application/json');
        }

        return $this->setContent($this->data);
    }
}