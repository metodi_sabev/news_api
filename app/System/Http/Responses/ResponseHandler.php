<?php

namespace app\System\Http\Responses;

class ResponseHandler
{
    public static function json($data = [], $status = 200, $headers = [], $options = 0)
    {
        return new JsonResponse($status, $data, $headers, $options);
    }
}