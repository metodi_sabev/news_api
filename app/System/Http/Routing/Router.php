<?php

namespace app\System\Http\Routing;

use app\System\Exceptions\NotFoundHttpException;

class Router
{
    protected $groupStack = [];

    protected $routes = [];

    protected $matchedRoutes = [];

    public function get($uri, $action = null)
    {
        $this->addRoute('GET', $uri, $action);
    }

    public function post($uri, $action = null)
    {
        $this->addRoute('POST', $uri, $action);
    }

    public function put($uri, $action = null)
    {
        $this->addRoute('PUT', $uri, $action);
    }

    public function patch($uri, $action = null)
    {
        $this->addRoute('PATCH', $uri, $action);
    }

    public function delete($uri, $action = null)
    {
        $this->addRoute('DELETE', $uri, $action);
    }

    public function options($uri, $action = null)
    {
        $this->addRoute('OPTIONS', $uri, $action);
    }

    protected function addRoute($method, $uri, $action)
    {
        if (!empty($this->groupStack['prefix'])) {
            $uri = $this->groupStack['prefix'] . '/' . trim($uri, "/");
        }

        $this->routes[] = new Route($method, $uri, $action);
    }

    public function getRoutes()
    {
        return $this->routes;
    }

    public function group($attributes, $callback)
    {
        $this->groupStack = $attributes;

        call_user_func($callback, $this);
    }

    public function handle(\app\System\Http\Request $request)
    {
        $uri = $request->getRewriteUri();
        $uri = $this->sanitizeURI($uri);
        $method = $request->getMethod();

        foreach ($this->routes as $key => $route) {
            if (!$route->isMethodAllowed($method)) {
                continue;
            }

            if ($route->matches($uri)) {
                $this->matchedRoutes[] = $route;
            }
        }

        if (empty($this->matchedRoutes)) {
            throw new NotFoundHttpException();
        }

        return $this->matchedRoutes;
    }

    protected function sanitizeURI($uri)
    {
        if ($uri != "/") {
            return rawurldecode(ltrim(rtrim($uri, "/"), "/"));
        }

        return "/";
    }
}
