<?php

namespace app\System\Http\Routing;

class Route
{
    protected $method;

    protected $uri;

    protected $action;

    protected $paramNames = [];

    protected $paramNamesPath;

    protected $params;

    public function __construct($method, $uri, $action)
    {
        $this->method = $method;
        $this->uri    = $uri;
        $this->action = $action;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function isMethodAllowed($method)
    {
        return $this->method == $method;
    }

    public function matches($resourceURI)
    {
        $patternAsRegex = preg_replace_callback(
            '#:([\w]+)\+?#',
            array($this, 'matchesCallback'),
            str_replace(')', ')?', (string)$this->uri)
        );

        if (substr($this->uri, -1) === '/') {
            $patternAsRegex .= '?';
        }

        if (!preg_match('#^' . $patternAsRegex . '$#', $resourceURI, $paramValues)) {
            return false;
        }

        foreach ($this->paramNames as $name) {
            if (isset($paramValues[$name])) {
                if (isset($this->paramNamesPath[$name])) {
                    $this->params[$name] = explode('/', urldecode($paramValues[$name]));
                } else {
                    $this->params[$name] = urldecode($paramValues[$name]);
                }
            }
        }

        return true;
    }

    public function matchesCallback($m)
    {
        $this->paramNames[] = $m[1];
        if (isset($this->conditions[$m[1]])) {
            return '(?P<' . $m[1] . '>' . $this->conditions[$m[1]] . ')';
        }
        if (substr($m[0], -1) === '+') {
            $this->paramNamesPath[$m[1]] = 1;

            return '(?P<' . $m[1] . '>.+)';
        }

        return '(?P<' . $m[1] . '>[^/]+)';
    }

    public function dispatch($request)
    {
        /**
         * @ToDo Define Middlewares and invoke them here
         */

        $response = null;
        if (is_string($this->action)) {

            $params['request'] = $request;
            if (!empty($this->params)) {
                foreach ($this->params as $key => $value) {
                    $params[$key] = $value;
                }
            }

            $response = call_user_func_array('app\Http\Controllers\\' . str_replace("@", "::", $this->action), $params);
        }

        if ($this->action instanceof \Closure) {
            $response = call_user_func($this->action);
        }

        return $response;
    }
}
