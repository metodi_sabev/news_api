<?php

namespace app\System\Http;

class Request
{
    public static $methods = [
        "GET",
        "POST",
        "PUT",
        "DELETE",
        "HEAD",
        "OPTIONS",
        "PATCH",
    ];

    public function getPost($var = null)
    {
        return $this->getHelper('_POST', $var);
    }

    public function getRaw()
    {
        return file_get_contents('php://input');
    }

    public function getPut($var = null)
    {
        return $this->getHelper('_PUT', $var);
    }

    public function getQuery($var = null)
    {
        return $this->getHelper('_GET', $var);
    }

    public function getServer($var = null)
    {
        return $this->getHelper('_SERVER', $var);
    }

    protected function getHelper($method, $var)
    {
        global $$method;

        if ($var) {
            return $$method[$var];
        }

        return $$method;
    }

    public function hasPost($var = null)
    {
        return $this->hasHelper('_POST', $var);
    }

    public function hasPut($var = null)
    {
        return $this->hasHelper('_PUT', $var);
    }

    public function hasQuery($var = null)
    {
        return $this->hasHelper('_GET', $var);
    }

    public function hasServer($var = null)
    {
        return $this->hasHelper('_SERVER', $var);
    }

    protected function hasHelper($method, $var)
    {
        if ($var) {
            return isset($$method[$var]);
        }

        return isset($$method);
    }

    public function getHeader($var = null)
    {
        return $this->getHelper('_SERVER', $var);
    }

    public function getScheme()
    {
        $isHttps = $this->getServer("HTTPS");

        if (!empty($isHttps)) {
            if ($isHttps != 'off') {
                return "https";
            }
        }

        return "http";
    }

    public function isSecureRequest()
    {
        return $this->getScheme() === "https";
    }

    public function getServerName()
    {
        if (isset($_SERVER['SERVER_NAME'])) {
            return $_SERVER['SERVER_NAME'];
        }

        return "localhost";
    }

    public function getServerPort()
    {
        if (isset($_SERVER['SERVER_PORT'])) {
            return $_SERVER['SERVER_PORT'];
        }

        return '';
    }

    public function getServerAddress()
    {
        if (isset($_SERVER['SERVER_ADDR'])) {
            return $_SERVER['SERVER_ADDR'];
        }

        return "localhost";
    }

    public function getHttpHost()
    {
        $httpHost = $_SERVER["HTTP_HOST"];

        if (!empty($httpHost)) {
            return $httpHost;
        }

        $scheme = $this->getScheme();
        $name = $this->getServerName();
        $port = $this->getServerPort();

        if ($scheme == 'http' && $port == 80) {
            return $name;
        }

        if ($scheme == 'https' && $port == "443") {
            return $name;
        }

        return $name . ':' . $port;
    }

    public function getURI()
    {
        $requestURI = $_SERVER["REQUEST_URI"];

        if (!empty($requestURI)) {
            return $requestURI;
        }

        return "/";
    }

    public function getRewriteURI()
    {
        $requestURI = $_SERVER["REQUEST_URI"];

        if (!empty($requestURI)) {
            return explode("?", $requestURI)[0];
        }
        
        return "/";
    }

    public function getClientAddress($trustForwardedHeader = false)
    {
        $address = '';

        if ($trustForwardedHeader) {
            $address = $_SERVER['HTTP_X_FORWARDED_FOR'];

            if (empty($address)) {
                $address = $_SERVER['HTTP_CLIENT_IP'];
            }
        }

        if (empty($address)) {
            $address = $_SERVER['REMOTE_ADDR'];
        }

        if (gettype($address) == 'string') {
            if (strpos($address, ',') !== false) {
                return explode(',', $address)[0];
            }
            return $address;
        }

        return false;
    }

    public function getMethod()
    {
        $method = $_SERVER["REQUEST_METHOD"];

        if (!empty($method)) {
            return $method;
        }

        return "";
    }

    public function getUserAgent()
    {
        if (!empty($_SERVER["HTTP_USER_AGENT"])) {
            return $_SERVER["HTTP_USER_AGENT"];
        }

        return "";
    }

    public function isValidHttpMethod($method)
    {
        return in_array($method, self::$methods);
    }

    public function isPost()
    {
        return $this->getMethod() === 'POST';
    }

    public function isGet()
    {
        return $this->getMethod() === 'GET';
    }

    public function isPut()
    {
        return $this->getMethod() === 'PUT';
    }

    public function isPatch()
    {
        return $this->getMethod() === 'PATCH';
    }

    public function isHead()
    {
        return $this->getMethod() === 'HEAD';
    }

    public function isDelete()
    {
        return $this->getMethod() === 'DELETE';
    }

    public function isOptions()
    {
        return $this->getMethod() === 'OPTIONS';
    }

    public function getHeaders()
    {
        return $_SERVER;
    }

    public function getHTTPReferer()
    {
        if (!empty($_SERVER["HTTP_REFERER"])) {
            return $_SERVER["HTTP_REFERER"];
        }

        return "";
    }
}
