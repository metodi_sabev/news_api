<?php

namespace app\System\Logger;

class Logger
{

    const DEBUG = 100;

    const INFO = 200;

    const NOTICE = 250;

    const WARNING = 300;

    const ERROR = 400;

    const CRITICAL = 500;

    const ALERT = 550;

    const EMERGENCY = 600;

    protected static $levels = [
        self::DEBUG     => 'DEBUG',
        self::INFO      => 'INFO',
        self::NOTICE    => 'NOTICE',
        self::WARNING   => 'WARNING',
        self::ERROR     => 'ERROR',
        self::CRITICAL  => 'CRITICAL',
        self::ALERT     => 'ALERT',
        self::EMERGENCY => 'EMERGENCY',
    ];

    protected function addRecord($level, $message)
    {
        $levelName = static::getLevelName($level);

        error_log($this->buildMessage($message, $levelName));
    }

    protected function buildMessage($message, $levelName)
    {
        return $levelName . ': ' . $message;
    }

    protected function getLevelName($level)
    {
        if (!isset(static::$levels[$level])) {
            throw new \Exception('Level "'.$level.'" is not defined, use one of: '.implode(', ', array_keys(static::$levels)));
        }

        return static::$levels[$level];
    }

    public function log($level, $message)
    {
        $this->addRecord($level, $message);
    }

    public function debug($message)
    {
        $this->addRecord(self::DEBUG, $message);
    }

    public function info($message)
    {
        $this->addRecord(self::INFO, $message);
    }

    public function notice($message)
    {
        $this->addRecord(self::NOTICE, $message);
    }

    public function warning($message)
    {
        $this->addRecord(self::WARNING, $message);
    }

    public function error($message)
    {
        $this->addRecord(self::ERROR, $message);
    }

    public function critical($message)
    {
        $this->addRecord(self::CRITICAL, $message);
    }

    public function alert($message)
    {
        $this->addRecord(self::ALERT, $message);
    }

    public function emergency($message)
    {
        $this->addRecord(self::EMERGENCY, $message);
    }
}