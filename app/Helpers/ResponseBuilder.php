<?php

namespace app\Helpers;

class ResponseBuilder
{
    public static function success($status = 200, $message = 'Success')
    {
        return [
            'code'    => $status,
            'message' => $message,
        ];
    }

    /**
     * Create Respons Array with Data
     *
     * @param mixed $data
     * @param int $status
     * @param string $message
     * @return array $response
     */
    public static function successWithData($data, $status = 200, $message = 'Success')
    {
        return [
            'code'    => $status,
            'message' => $message,
            'data'    => $data,
        ];
    }

    public static function failure($status = 400, $message = 'Bad request')
    {
        if (is_array($message)) {
            return [
                'code'  => $status,
                'error' => $message,
            ];
        }

        return [
            'code'  => $status,
            'error' => [[
                'message' => $message,
            ]],
        ];
    }

    public static function error($status = 500, $message = 'Internal server error')
    {
        if (is_array($message)) {
            return [
                'code'  => $status,
                'error' => $message,
            ];
        }
        
        return [
            'code'  => $status,
            'error' => [[
                'message' => $message,
            ]],
        ];
    }
}
