<?php

namespace app\Models;

use app\System\DB\Model;

class Articles extends Model
{
    protected $tableName = 'news_articles';

    protected $attributes = [];

    protected $fillable = [
        'id',
        'headline',
        'content',
    ];
}