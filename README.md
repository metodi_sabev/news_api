## A simple micro framework with basic news api implementation ##

### Requirements ###
* PHP 5.5.9+

### Virtual Hosts ###


```
#!Virtual hosts

<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	ServerName example.com
	DocumentRoot /var/www/example/public

	<Directory /var/www/example/public>
                AllowOverride All
                Allow from all
	</Directory>
</VirtualHost>

```

### Additional Configurations ###
The app needs almost no other configuration out of the box. However, you may wish to rename the .env.example file to .env and set some of the options, as well as to configure your MySQL credentials.




